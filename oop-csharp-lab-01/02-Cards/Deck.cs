﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        { 
        }

    public void Initialize()
    {
      /*
       * == README ==
       * 
       * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
       * di un tipo mazzo di carte da gioco italiano.
       * 
       * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
       * 
       * Nota - Dato un generico enumerato MyEnum:
       *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
       *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
       * 
       */
      cards = new Card[40];
      Populate();
    }

    private void Populate()
    {
      for (int seme = 0; seme < Enum.GetNames(typeof(ItalianSeed)).Length; seme++)
      {
        for (int valore = 0; valore < Enum.GetNames(typeof(ItalianValue)).Length; valore++)
        {
          cards[seme * 10 + valore] = new Card(valore.ToString(), seme.ToString());
        }
      }

    }

    public void Print()
    {
      /*
       * == README  ==
       * 
       * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
       */

      foreach (Card c in cards)
      {
        Console.WriteLine("Seme: " + Enum.Parse(typeof(ItalianSeed), c.GetSeed()) + "; Valore: " + Enum.Parse(typeof(ItalianValue), c.GetValue()));
      }
    }

    public Card this[ItalianSeed seme, ItalianValue valore] => cards[Convert.ToInt32(seme) * 10 + Convert.ToInt32(valore)];

  }


  enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
