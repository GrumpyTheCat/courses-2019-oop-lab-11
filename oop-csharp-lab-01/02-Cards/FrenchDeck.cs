﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Cards
{
  class FrenchDeck
  {

    private Card[] cards;
    const int DECK_DIMENSION = 52;
    const int JOLLY_NUMBER = 4;

    public void Initialize()
    {
      cards = new Card[DECK_DIMENSION];
      Populate();
    }

    private void Populate()
    {
      for (int seme = 0; seme < Enum.GetNames(typeof(FrenchSeed)).Length; seme++)
      {
        for (int valore = 0; valore < Enum.GetNames(typeof(FrenchValue)).Length; valore++)
        {
          cards[seme * 13 + valore] = new Card(valore.ToString(), seme.ToString());
        }
      }
      for(int j = 0; j<JOLLY_NUMBER; j++)
      {
        cards[DECK_DIMENSION + j] = new Card("Jolly", "4");
      }
    }
  }

  enum FrenchSeed
  {
    CUORI,
    FIORI,
    QUADRI,
    PICCHE
  }

  enum FrenchValue
  {
    ASSO,
    DUE,
    TRE,
    QUATTRO,
    CINQUE,
    SEI,
    SETTE,
    OTTO,
    NOVE,
    DIECI,
    JACK,
    DONNA,
    RE
  }


}
