﻿using System;

namespace ComplexNumbers
{
  class ComplexNum
  {
    public ComplexNum(double re, double im)
    {
      Re = re;
      Im = im;
    }

    public double Re { get; private set; }
    public double Im { get; private set; }


    // Restituisce il modulo del numero complesso
    public double Module
    {
      get
      {
        return Math.Sqrt(Math.Pow(Re, 2) + Math.Pow(Im, 2));
      }
    }

    // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
    public ComplexNum Conjugate
    {
      get
      {
        return new ComplexNum(this.Re, -this.Im);
      }
    }

    // Restituisce una rappresentazione idonea per il numero complesso
    public override string ToString()
    {
      return Re.ToString() + (Im > 0 ? " + " : " - ") + "i" + Math.Abs(Im).ToString();
    }

    public static ComplexNum operator + (ComplexNum c1, ComplexNum c2)
    {
      return new ComplexNum(c1.Re + c2.Re, c1.Im + c2.Im);
    }

    public static ComplexNum operator -(ComplexNum c1, ComplexNum c2)
    {
      return new ComplexNum(c1.Re - c2.Re, c1.Im - c2.Im);
    }

    public static ComplexNum operator *(ComplexNum c1, ComplexNum c2)
    {
      return new ComplexNum((c1.Re * c2.Re) - (c1.Im * c2.Im), (c1.Re * c2.Im) + (c1.Im * c2.Re));
    }

    public static ComplexNum operator /(ComplexNum c1, ComplexNum c2)
    {
      throw new NotImplementedException();
    }


  }
}
